//
//  Networking.swift
//  VersionReporter
//
//  Created by Peter Eddy on 10/31/20.
//

import SwiftUI
import Network

//let networkMonitor = NetworkMonitor()

class NetworkMonitor {
  
  let monitor = NWPathMonitor()
  let queue = DispatchQueue(label: "Monitor")
  var connected = false {
    didSet {
      startTimer()
    }
  }
  
//  var environments: FetchedResults<VersionReporter.Environment>?
  
  weak var timer: Timer?
  
//  init(environments: Binding<VersionReporter.Environment>) {
//    self.environments = environments
//  }
  
  func start() {
    self.monitor.pathUpdateHandler = { path in
      self.connected = (path.status == .satisfied)
      print("Networking Connected? \(self.connected), expensive? \(path.isExpensive)")
    }

    monitor.start(queue: self.queue)
  }
  
  func startTimer() {
    stopTimer()
    guard connected else { return }
//    timer = Timer.scheduledTimer(withTimeInterval: s, repeats: , block: <#T##(Timer) -> Void#>)
  }
  
  func stopTimer() {
    timer?.invalidate()
  }
  
  func request(url: URL, user: String?, password: String?, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
    let session = URLSession.shared
//    let url = URL(string: "https://www.boston.com")!
    var request = URLRequest(url: url)

    let login = String(format: "%@:%@", "admin", "admin")
    if let data = login.data(using: .utf8) {
      let loginString = data.base64EncodedString()
      request.setValue("Basic \(loginString)", forHTTPHeaderField: "Authorization")
    }

    let task = session.dataTask(with: request) { (data, response, error) in
      print("response \(String(describing: response))")
      print("error: \(String(describing: error))")
      completionHandler(data, response, error)
    }
    task.resume()
//    let task = session.dataTask(with: request, completionHandler: completionHandler) { (data, response, error) in
//      print("response \(response)")
//      print("error: \(error)")
//    }
//    task.resume()
  }
}
