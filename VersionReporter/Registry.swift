//
//  Registry.swift
//  VersionReporter
//
//  Created by Peter Eddy on 10/24/20.
//

import Cocoa
import SwiftUI

let registryName = "Registry"

extension Registry {

  static var context: NSManagedObjectContext {
    AppDelegate.context!
  }

  static func createNewRegistry(name: String, url: String) -> Registry {
    let registry = Registry(context: context)
    registry.id = UUID()
    registry.name = name
//    let uc = URLComponents(string: url)
//    registry.url = uc?.url
    registry.url = url
    return registry
  }

  static var registryCount: Int {
    let registryFetchRequst = NSFetchRequest<NSFetchRequestResult>(entityName: registryName)
    do {
      return try context.fetch(registryFetchRequst).count
    }
    catch {
      return 0
    }
  }

  static func createDefaultRegistries() {
    // _ = deleteAll(entityName: registryName)
    guard registryCount == 0 else { return }
    print("creating default registries")
    _ = createNewRegistry(name: "Public",
                          url: "http://oracle-cx-commerce-repository.occa.ocs.oraclecloud.com")
    _ = createNewRegistry(name: "Development",
                          url: "https://pnpmdev-npmregistry.occa.us-phoenix-1.ocs.oraclecloud.com")
    _ = createNewRegistry(name: "Test",
                          url: "https://pnpmtst-npmregistry.occa.us-phoenix-1.ocs.oraclecloud.com")
    _ = createNewRegistry(name: "Production",
                          url: "https://pnpmprd-npmregistry.occa.us-phoenix-1.ocs.oraclecloud.com")
    _ = createNewRegistry(name: "Internal",
                          url: "https://pnpmliveprd-npmregistry.occa.us-phoenix-1.ocs.oc-test.com")
    do {
      try context.save()
      print("saved default registries")
    }
    catch {
      print("Failed to save default registries")
      print(error.localizedDescription)
    }
  }
}
