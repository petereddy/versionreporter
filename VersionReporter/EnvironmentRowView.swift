//
//  EnvironmentRow.swift
//  VersionReporter
//
//  Created by Peter Eddy on 10/25/20.
//

import SwiftUI

struct EnvironmentRowView: View {
  
  @ObservedObject var environment: VersionReporter.Environment
  @Binding var selectedEnvironment: VersionReporter.Environment?
  @Binding var showEnvironmentForm: Bool
  
  var body: some View {
    GeometryReader { geometry in
      HStack {
        Text(environment.name ?? "")
          .multilineTextAlignment(.leading)
          .frame(width: geometry.size.width * 0.5, alignment: .leading)
        Text(environment.version ?? "<None>")
          .multilineTextAlignment(.leading)
          .frame(width: geometry.size.width * 0.5, alignment: .leading)
      }
      .onTapGesture(count: 2) {
        self.selectedEnvironment = self.environment
        self.showEnvironmentForm = true
      }
      .onTapGesture(count: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/) {
        self.selectedEnvironment = self.environment
      }
    }
  }
}
