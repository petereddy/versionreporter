//
//  EnvironmentForm.swift
//  VersionReporter
//
//  Created by Peter Eddy on 10/25/20.
//

import SwiftUI

struct EnvironmentFormView: View  {
  
  @State private var envName = ""
  @State private var envURL = ""
  @State private var envUser = ""
  @State private var envPass = ""

  private var environment: VersionReporter.Environment?
  @SwiftUI.Environment(\.presentationMode) var presentationMode

  init(environment: VersionReporter.Environment? = nil) {
    self.environment = environment
    if let env = self.environment {
      self._envName = State(initialValue: env.name ?? "")
      self._envURL = State(initialValue: env.url ?? "")
      self._envUser = State(initialValue: env.user ?? "")
      self._envPass = State(initialValue: env.password ?? "")
    }
  }

  func validURL() -> Bool {
    URL(string: envURL) != nil
  }
  
  var body: some View {
    Form {
      Section(header: Text("Environment Name")) {
        TextField("Name", text: $envName)
      }
      Section(header: Text("Storefront URL")) {
        TextField("URL", text: $envURL)
      }
      Section(header: Text("Authentication (defaults to admin/admin)")) {
        HStack {
          TextField("User Name", text: $envUser)
          SecureField("Password", text: $envPass)
        }
      }
      Section {
        HStack {
          Button(action: {
            AppDelegate.context.rollback()
            self.presentationMode.wrappedValue.dismiss()
          }) {
            Text("Cancel")
          }
          Button(action: {
            save()
          }) { Text("Save") }
          .disabled(!validURL() || envName.isEmpty)
        }
      }
    }
    .frame(width: 400.0, height: 200)
    .padding()
  }

  func printCoreDataError(error: Error) {
    print(error.localizedDescription)
    let saveError = error as NSError
    if let multiError:[NSError] = saveError.userInfo[NSDetailedErrorsKey] as? [NSError] {
      print("e: \(multiError.debugDescription)")
    }
  }

  func save() {
    let env = self.environment ?? VersionReporter.Environment(context: AppDelegate.context)
    env.name = self.envName
    env.url = self.envURL
    env.user = self.envUser
    env.password = self.envPass
    if env.id == nil {
      env.id = UUID()
    }
    do {
      try AppDelegate.context.save()
      self.presentationMode.wrappedValue.dismiss()
    }
    catch {
      printCoreDataError(error: error)
    }
  }
}
