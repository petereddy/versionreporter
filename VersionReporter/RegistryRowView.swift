//
//  RegistryRow.swift
//  VersionReporter
//
//  Created by Peter Eddy on 10/25/20.
//

import SwiftUI

struct RegistryRowView: View {
  
  let registry: Registry
  
  var body: some View {
    GeometryReader { geometry in
      HStack {
        Text(registry.name ?? "")
          .frame(width: geometry.size.width / 2, alignment: .leading)
        Text(registry.version ?? "<None>")
          .frame(width: geometry.size.width * 0.5, alignment: .leading)
      }
    }
  }
}

struct RegistryRow_Previews: PreviewProvider {
  static var previews: some View {
    //RegistryRow(Registry())
    Text("Hello")
  }
}
