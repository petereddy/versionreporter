//
//  EnvironmentFormView.swift
//  VersionReporter
//
//  Created by Peter Eddy on 10/26/20.
//

import SwiftUI

struct EnvironmentHeaderView: View {

  @SwiftUI.Environment(\.managedObjectContext) var managedObjectContext
  @State private var showEnvironmentForm = false
  @SwiftUI.Environment(\.selectedEnvironment)
    var selectedEnvironment: VersionReporter.Environment?

  var body: some View {
    HStack {
      Text("Environments")
      HStack(spacing: 0) {
        Button(action: {
          self.showEnvironmentForm = true
        }) {
          Image(nsImage: NSImage(named: NSImage.addTemplateName)!)
            .resizable()
            .scaledToFit()
        }
        .popover(isPresented: self.$showEnvironmentForm, content: {
          EnvironmentFormView()
        })
        Button(action: {
          deleteSelectedObject()
        }) {
          Image(nsImage: NSImage(named: NSImage.removeTemplateName)!)
            .resizable()
            .scaledToFit()
        }
        .disabled(selectedEnvironment == nil)
      }
    }
  }
  
  func deleteSelectedObject() {
    if let env = selectedEnvironment {
      self.managedObjectContext.delete(env)
      try? AppDelegate.context.save()
    }
  }
}
