//
//  ContentView.swift
//  VersionReporter
//
//  Created by Peter Eddy on 10/18/20.
//

import SwiftUI

private struct SelectedEnvironmentKey: EnvironmentKey {
  static let defaultValue: VersionReporter.Environment? = nil
}

extension EnvironmentValues {
  var selectedEnvironment: VersionReporter.Environment? {
    get { self[SelectedEnvironmentKey.self] }
    set { self[SelectedEnvironmentKey.self] = newValue }
  }
}

struct ContentView: View {

  @SwiftUI.Environment(\.managedObjectContext) var managedObjectContext
  @State private var selection: VersionReporter.Environment? = nil
  @State private var showEnvironmentForm = false
  
  // sqlight data in
  // /Users/petere/Library/Containers/com.petereddy.VersionReporter/Data/Library/Application Support/VersionReporter
  @FetchRequest(
    entity: Registry.entity(),
    sortDescriptors: [
      NSSortDescriptor(keyPath: \Registry.id, ascending: true)
    ]
  ) var registries: FetchedResults<Registry>
  
  @FetchRequest(
    entity: VersionReporter.Environment.entity(),
    sortDescriptors: [
      NSSortDescriptor(keyPath: \VersionReporter.Environment.name,
                       ascending: true)]
  ) var environments: FetchedResults<VersionReporter.Environment>

  var body: some View {
    VStack(alignment: .leading) {
      EnvironmentHeaderView()
        .environment(\.selectedEnvironment, selection)
        .environment(\.managedObjectContext, managedObjectContext)
      List(environments, id: \.self, selection: $selection) { environment in
        EnvironmentRowView(environment: environment,
                           selectedEnvironment: self.$selection,
                           showEnvironmentForm: self.$showEnvironmentForm)
      }
      .popover(isPresented: self.$showEnvironmentForm) {
        EnvironmentFormView(environment: selection)
      }
      .onMoveCommand { direction in
        print("move direction \(direction)")
      }
      .onDeleteCommand {
        print("Delete")
      }
      Section {
        Text("Registries")
        List(registries) {
          RegistryRowView(registry: $0)
        }
        HStack {
          Button(action: {
          }) {
            Image(nsImage: NSImage(named: NSImage.actionTemplateName)!)
              .resizable()
              .scaledToFit()
          }
          Button(action: {
            exit(0)
          }) {
            Text("Quit")
          }
        }
      }
    }
    .padding()
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    let container = NSPersistentContainer(name: "VersionReporter")
    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
      if let error = error {
        fatalError("Unresolved error \(error)")
      }
    })
    return ContentView().environment(\.managedObjectContext, container.viewContext)
  }
}
